#!/bin/bash
set -e
set -x

# Debug printing
whoami
env | sort

if [ -z "${HBP}" ]; then
    echo "USAGE: The HBP variable not specified"
    exit 1
fi

USER_SCRIPTS_DIR=${USER_SCRIPTS_DIR:-"user-scripts"}
EXP_CONTROL_DIR=${EXP_CONTROL_DIR:-"ExperimentControl"}
CLE_DIR=${CLE_DIR:-"CLE"}
BRAIN_SIMULATION_DIR=${BRAIN_SIMULATION_DIR:-"BrainSimulation"}
EXDBACKEND_DIR=${EXDBACKEND_DIR:-"ExDBackend"}

export PYTHONPATH=

source "${HBP}/${USER_SCRIPTS_DIR}/nrp_variables"

cd "${HBP}/${EXP_CONTROL_DIR}"

# Configure build

export NRP_INSTALL_MODE=dev
export IGNORE_LINT='build_venv|platform_venv|nest'

# verify_base-ci fails on dependencies mismatch, but ignores linter errors, which are cought by Jenkins afterwards
rm -rf build_env
virtualenv build_env \
    && . build_env/bin/activate \
    && make --always-make verify_base-ci
