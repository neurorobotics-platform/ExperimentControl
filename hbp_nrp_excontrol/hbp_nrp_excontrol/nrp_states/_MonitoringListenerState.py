# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains state implementations that listen to monitoring messages
"""

__author__ = 'Georg Hinkel'

import smach_ros
from cle_ros_msgs.msg import SpikeEvent, SpikeRate


class MonitorRateState(smach_ros.MonitorState):
    """
    This class catches the monitoring information sent from the CLE
    """

    # pylint: disable=no-self-use
    def _topic_name(self):  # pragma: no cover
        """
        Gets the topic for this monitor state

        :return: The topic name for this monitor state as a string
        """
        raise Exception("This method must be implemented by derived classes")

    def __callback(self, user_data, spike_rate):
        """
        This method gets executed when a new spike count arrives

        :param user_data: The user data for the current state machine
        :param spike_rate: The current spike rate
        """
        if spike_rate.monitorName == self.__monitor_name:
            return self.__monitor_cb(user_data, spike_rate.simulationTime, spike_rate.rate)
        return True

    def __init__(self, monitor_name, monitor_cb):
        """
        Creates a new state that monitors spike rates

        :param monitor_name: The name of the monitor
        :param monitor_cb: A method that should be called when new monitoring data is available
        """
        super(MonitorRateState, self).__init__(self._topic_name(), SpikeRate,
                                               self.__callback)
        self.__monitor_name = monitor_name
        self.__monitor_cb = monitor_cb


class MonitorSpikeRateState(MonitorRateState):
    """
    This class catches the monitoring information sent from the CLE
    """

    def _topic_name(self):
        """
        Gets the topic for this monitor state

        :return: The topic name for this monitor state as a string
        """
        return '/monitor/population_rate'

    def __init__(self, monitor_name, monitor_cb):
        """
        Creates a new state that monitors spike rates

        :param monitor_name: The name of the monitor
        :param monitor_cb: A method that should be called when new monitoring data is available
        """
        super(MonitorSpikeRateState, self).__init__(monitor_name, monitor_cb)


class MonitorLeakyIntegratorAlphaState(MonitorRateState):
    """
    This class catches the monitoring information sent from the CLE
    """

    def _topic_name(self):
        """
        Gets the topic for this monitor state

        :return: The topic name for this monitor state as a string
        """
        return '/monitor/leaky_integrator_alpha'

    def __init__(self, monitor_name, monitor_cb):
        """
        Creates a new state that monitors spike rates

        :param monitor_name: The name of the monitor
        :param monitor_cb: A method that should be called when new monitoring data is available
        """
        super(MonitorLeakyIntegratorAlphaState, self).__init__(monitor_name, monitor_cb)


class MonitorLeakyIntegratorExpState(MonitorRateState):
    """
    This class catches the monitoring information sent from the CLE
    """

    def _topic_name(self):
        """
        Gets the topic for this monitor state

        :return: The topic name for this monitor state as a string
        """
        return '/monitor/leaky_integrator_exp'

    def __init__(self, monitor_name, monitor_cb):
        """
        Creates a new state that monitors spike rates

        :param monitor_name: The name of the monitor
        :param monitor_cb: A method that should be called when new monitoring data is available
        """
        super(MonitorLeakyIntegratorExpState, self).__init__(monitor_name, monitor_cb)


class MonitorSpikeRecorderState(smach_ros.MonitorState):
    """
    This class catches the monitoring information sent from the CLE
    """

    def __callback(self, user_data, spike_event):
        """
        This method gets executed when a new spike count arrives

        :param user_data: The user data for the current state machine
        :param spike_event: The current spike rate
        """
        if spike_event.monitorName == self.__monitor_name:
            return self.__monitor_cb(user_data, spike_event.simulationTime, spike_event.spikes)
        return True

    def __init__(self, monitor_name, monitor_cb):
        """
        Creates a new state that monitors spike rates

        :param monitor_name: The name of the monitor
        :param monitor_cb: A method that should be called when new monitoring data is available
        """
        super(MonitorSpikeRecorderState, self).__init__('/monitor/spike_recorder', SpikeEvent,
                                                        self.__callback)
        self.__monitor_name = monitor_name
        self.__monitor_cb = monitor_cb
