# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains classes to interact with the simulation lifecycle
"""

import smach
import rospy
from cle_ros_msgs.msg import SimulationLifecycleStateChange
from hbp_nrp_excontrol import sim_id, sm_id
from hbp_nrp_excontrol.logs import clientLogger

__author__ = 'Hossain Mahmud, Georg Hinkel'


class StopSimulation(smach.State):
    """
    Defines a state that stops the simulation
    """

    def __init__(self):
        """
        Creates a new state that stops the simulation
        """
        super(StopSimulation, self).__init__(outcomes=['succeeded', 'failed'])
        # We need a permanent publisher due to the asynchronous nature of ROS
        self.__pub = rospy.Publisher('/ros_cle_simulation/' + str(sim_id) + '/lifecycle',
                                     SimulationLifecycleStateChange,
                                     queue_size=0)

    # pylint: disable=unused-argument,arguments-differ
    def execute(self, userdata):
        """
        Executes the stop simulation state

        :param userdata: The user data dictionary is not important for this state
        :return: The possible outcomes of this state are succeeded and failed
        """
        try:
            clientLogger.info("SM {} requested to stop the simulation".format(sm_id))
            self.__pub.publish(rospy.get_caller_id(), "started", "stopped", "stopped")
            self.__pub.unregister()
            return 'succeeded'
        # pylint: disable=broad-except
        except Exception as e:
            clientLogger.info("Failed: " + str(e))
            return 'failed'
