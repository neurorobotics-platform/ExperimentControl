# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains classes to dim the lights in the virtual environment
"""

import rospy
import smach
import smach_ros
from std_msgs.msg import ColorRGBA
from gazebo_msgs.srv import SetLightProperties, GetLightProperties, SetLightPropertiesRequest

__author__ = 'Georg Hinkel'


class LightServiceState(smach_ros.ServiceState):
    """
    Defines a service state that sets the light
    """

    # pylint: disable=unused-argument
    @smach.cb_interface()
    def __request_callback(self, userdata, request):  # pragma: no cover
        """
        Callback when light should be changed

        :param userdata: The simulation user data
        :param request: The original request
        """

        return self.create_service_request()

    def create_service_request(self):
        """
        Creates the service request object
        """
        diffuse = self.__diffuse
        attenuation_constant = self.__attenuation_constant
        attenuation_linear = self.__attenuation_linear
        attenuation_quadratic = self.__attenuation_quadratic
        if isinstance(diffuse, list):
            diffuse = ColorRGBA(diffuse[0], diffuse[1], diffuse[2], diffuse[3])
        if diffuse is None or attenuation_constant is None \
                or attenuation_linear is None or attenuation_quadratic is None:

            get_light_properties = None
            try:
                while not rospy.is_shutdown() and get_light_properties is None:
                    if self.preempt_requested(): # pragma: no cover
                        rospy.loginfo("Preempting while waiting for service "
                                      "'/gazebo/get_light_properties'")
                        self.service_preempt()
                        return None
                    try:
                        rospy.wait_for_service(self._service_name, 1.0)
                        get_light_properties = rospy.ServiceProxy('/gazebo/get_light_properties',
                                                                  GetLightProperties)
                        rospy.logdebug("Connected to service '/gazebo/get_light_properties'")
                    # pylint: disable=unused-variable
                    except rospy.ROSException as ex: # pragma: no cover
                        rospy.logwarn("Still waiting for service '/gazebo/get_light_properties'")
            # pylint: disable=broad-except
            except Exception:  # pragma: no cover
                rospy.logwarn("Terminated while waiting for service "
                              "'/gazebo/get_light_properties'")
                return None

            light_properties = get_light_properties(light_name=self.__name)

            if attenuation_constant is None:
                attenuation_constant = light_properties.attenuation_constant
            if attenuation_linear is None:
                attenuation_linear = light_properties.attenuation_linear
            if attenuation_quadratic is None:
                attenuation_quadratic = light_properties.attenuation_quadratic
            if diffuse is None:
                diffuse = light_properties.diffuse
        return SetLightPropertiesRequest(light_name=self.__name,
                                         diffuse=diffuse,
                                         attenuation_constant=attenuation_constant,
                                         attenuation_linear=attenuation_linear,
                                         attenuation_quadratic=attenuation_quadratic)

    def __init__(self, name, diffuse=None, attenuation_constant=None, attenuation_linear=None,
                 attenuation_quadratic=None):
        """
        Creates a new service state to set the light

        :param name: The name of the light source that shall be triggered
        :param diffuse: The diffuse of the light. If not set, the diffuse is not overridden
        :param attenuation_constant: The constant attenuation. If not set, it is not overridden
        :param attenuation_linear: The linear attenuation. If not set, it is not overridden
        :param attenuation_quadratic: The quadratic attenuation. If not set, it is not overridden
        """
        super(LightServiceState, self).__init__('/gazebo/get_light_properties', SetLightProperties,
                                                request_cb=self.__request_callback)
        self.__name = name
        self.__diffuse = diffuse
        self.__attenuation_constant = attenuation_constant
        self.__attenuation_linear = attenuation_linear
        self.__attenuation_quadratic = attenuation_quadratic

        if isinstance(diffuse, dict):
            self.__diffuse = [diffuse.get('r'), diffuse.get('g'),
                              diffuse.get('b'), diffuse.get('a')]
