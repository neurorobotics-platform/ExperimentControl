# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains classes to set materials in the virtual environment
"""

import smach_ros
import smach
import rospy
import tf
from gazebo_msgs.srv import SpawnEntity, SpawnEntityRequest, DeleteModel, SetModelState, \
    GetModelState, SetModelStateRequest
from gazebo_msgs.msg import ModelState
from geometry_msgs.msg import Point, Vector3, Quaternion, Pose

__author__ = 'Sven Liedtke, Georg Hinkel'


def create_cb(result_cb, *components):
    """
    Creates a Smach callback from the inner callbacks

    :param result_cb: The result callback function
    :param components: The component callbacks
    """
    input_keys = []
    output_keys = []
    for cb in components:
        if hasattr(cb, '_input_keys'):
            input_keys += getattr(cb, '_input_keys')
            output_keys += getattr(cb, '_output_keys')
    return smach.cb_interface(input_keys=input_keys, output_keys=output_keys)(result_cb)


class ModelSpawnServiceState(smach_ros.ServiceState):
    """
    Defines a state that monitors the simulation time
    """

    # pylint: disable=too-many-arguments
    def __init__(self, model_name, geometry, position, rotation, can_collide, gravity_factor=1,
                 kinematic=1, mass=1, color='Gazebo/Grey'):
        """
        Creates a new state that can spawn models

        :param model_name: A given name for the new object that is spawned
        :param geometry: SDF Geometry specifier
        :param position: Vector3 with the initial position of the new object
        :param rotation: Vector3 in radians with the initial orientation fo the new object
        """

        if gravity_factor is True:
            gravity_factor = 1
        elif gravity_factor is False:
            gravity_factor = 0
        if kinematic is True:
            kinematic = 1
        elif kinematic is False:
            kinematic = 0

        collision = '<inertial>' \
                    + '<mass>' + str(mass) + '</mass>' \
                    + '  <inertia>' \
                    + '    <ixx>1</ixx>' \
                    + '    <ixy>0</ixy>' \
                    + '    <ixz>0</ixz>' \
                    + '    <iyy>1</iyy>' \
                    + '    <iyz>0</iyz>' \
                    + '    <izz>1</izz>' \
                    + '  </inertia>' \
                    + '</inertial>' \
                    + '<collision name="collision">' \
                    + '  <geometry>' + geometry + '</geometry>' \
                    + '  <max_contacts> 10 </max_contacts>' \
                    + '  <surface>' \
                    + '    <contact>' \
                    + '      <ode/>' \
                    + '    </contact>' \
                    + '    <bounce/>' \
                    + '    <friction>' \
                    + '      <torsional>' \
                    + '        <ode/>' \
                    + '      </torsional >' \
                    + '      <ode/>' \
                    + '    </friction >' \
                    + '  </surface >' \
                    + '</collision>'
        model = '<?xml version="1.0" ?>' \
                + '<sdf version="1.5">' \
                + '  <model name="placeholder">' \
                + '    <pose>0 0 0 0 0 0</pose>' \
                + '    <static>0</static>' \
                + '    <link name="body">' \
                + (collision if can_collide else '') \
                + '      <visual name="visual">' \
                + '        <cast_shadows>1</cast_shadows>' \
                + '        <geometry>' + geometry + '</geometry>' \
                + '        <material><script>' \
                + '           <uri>file://media/materials/scripts/gazebo.material</uri>' \
                + '           <name>' + color + '</name>' \
                + '        </script></material>' \
                + '      </visual>' \
                + '      <self_collide>0</self_collide>' \
                + '      <kinematic>' + str(kinematic) + '</kinematic>' \
                + '      <gravity>' + str(gravity_factor) + '</gravity>' \
                + '    </link>' \
                + '  </model>' \
                + '</sdf>'
        qRot = tf.transformations.quaternion_from_euler(rotation.x, rotation.y,
                                                        rotation.z)
        if callable(model_name) or callable(position):
            self.model_name_cb = model_name if callable(model_name) else lambda ud, _: model_name
            self.position_cb = position if callable(position) else lambda ud, _: position
            self.model = model
            self.__model_name = None
            self.__position = None
            self.__q_rot = Quaternion(*qRot)
            super(ModelSpawnServiceState, self).__init__('/gazebo/spawn_sdf_entity', SpawnEntity,
                                                         request_cb=create_cb(self.__request_cb,
                                                                              self.model_name_cb,
                                                                              self.position_cb))
        else:
            pose = Pose(Point(position.x, position.y, position.z), Quaternion(*qRot))
            super(ModelSpawnServiceState, self).__init__('/gazebo/spawn_sdf_entity', SpawnEntity,
                                                         request=SpawnEntityRequest(model_name,
                                                                                    model,
                                                                                    "",
                                                                                    pose,
                                                                                    'world'))

    def __request_cb(self, ud, request):
        """
        Create a new request depending on the provided callbacks

        :param ud: The user data
        :param request: The previous request
        :return: The new request
        """
        self.__model_name = self.model_name_cb(ud, self.__model_name)
        self.__position = self.position_cb(ud, self.__position)
        pose = Pose(Point(self.__position.x, self.__position.y, self.__position.z), self.__q_rot)
        return SpawnEntityRequest(self.__model_name,
                                  self.model,
                                  "",
                                  pose,
                                  'world')


class ModelDestroyServiceState(smach_ros.ServiceState):
    """
    Defines a state that monitors the simulation time
    """

    def __init__(self, model_name):
        """
        Creates a new state that can destroy models

        :param model_name: The object's name that should be destroyed
        """
        if callable(model_name):
            super(ModelDestroyServiceState, self).__init__('/gazebo/delete_model', DeleteModel,
                                                           request_cb=model_name)
        else:
            super(ModelDestroyServiceState, self).__init__('/gazebo/delete_model', DeleteModel,
                                                           request=model_name)


class SetModelServiceState(smach_ros.ServiceState):
    """
    Defines a state that monitors the simulation time
    """

    def __init__(self, model_name, position, rotation, scale):
        """
        Creates a new state that can spawn models

        :param model_name: A given name for the new object that is spawned
        :param position: Vector3 with the new position of an object
        :param rotation: Vector3 in radians with the initial orientation fo the new object
        :param position: Vector3 with the new scale of an object
        """
        if callable(model_name) or callable(position) or callable(rotation) or callable(scale):
            self.model_name_cb = model_name if callable(model_name) else lambda ud, _: model_name
            self.__model_name = None
            self.scale_cb = scale if callable(scale) else lambda ud, _: scale
            self.__scale = None
            self.position_cb = position if callable(position) else lambda ud, _: position
            self.__position = None
            self.rotation_cb = rotation if callable(rotation) else lambda ud, _: rotation
            self.__rotation = None
            super(SetModelServiceState, self).__init__('/gazebo/set_model_state', SetModelState,
                                                       request_cb=create_cb(self.__request_cb,
                                                                            self.model_name_cb,
                                                                            self.scale_cb,
                                                                            self.position_cb,
                                                                            self.rotation_cb))
        else:
            msg = ModelState()
            msg.model_name = model_name
            msg.scale = scale
            msg.pose.position = Point(position.x, position.y, position.z)
            qRot = tf.transformations.quaternion_from_euler(rotation.x, rotation.y, rotation.z)
            msg.pose.orientation = Quaternion(*qRot)
            msg.reference_frame = 'world'
            super(SetModelServiceState, self).__init__('/gazebo/set_model_state', SetModelState,
                                                       request=msg)

    def __request_cb(self, ud, request):
        """
        Create a new request for the given user data

        :param ud: The state machine user data
        :param request: The current request
        :return: The new request
        """
        self.__model_name = self.model_name_cb(ud, self.__model_name)
        self.__position = self.position_cb(ud, self.__position)
        self.__rotation = self.rotation_cb(ud, self.__rotation)
        self.__scale = self.scale_cb(ud, self.__scale)

        msg = ModelState()
        msg.model_name = self.__model_name
        msg.scale = self.__scale
        pos = self.__position
        msg.pose.position = Point(pos.x, pos.y, pos.z)
        rot = self.__rotation
        qRot = tf.transformations.quaternion_from_euler(rot.x, rot.y,
                                                        rot.z)
        msg.pose.orientation = Quaternion(*qRot)
        msg.reference_frame = 'world'
        return msg


class SpawnSphere(ModelSpawnServiceState):
    """
    Defines a state that can spawn sphere objects
    """

    # pylint: disable=too-many-arguments
    def __init__(self, model_name, radius=1, position=Vector3(0, 0, 0),
                 rotation=Vector3(0, 0, 0), can_collide=False, gravity_factor=1,
                 kinematic=False, mass=1, color='Gazebo/Grey'):
        """
        Defines a state that creates a simple sphere object in the world

        :param model_name: A given name for the new object that is spawned
        :param radius: Number that specifies the radius of the sphere
        :param position: Vector3 with the initial position of the new object
        :param rotation: Vector3 in radians with the initial orientation fo the new object
        """
        geometry = '<sphere><radius>' + str(radius) + '</radius></sphere>'
        super(SpawnSphere, self).__init__(model_name, geometry, position, rotation, can_collide,
                                          gravity_factor, kinematic, mass, color)


class SpawnCylinder(ModelSpawnServiceState):
    """
    Defines a state that can spawn cylinder objects
    """

    # pylint: disable=too-many-arguments
    def __init__(self, model_name, length=1, radius=0.25, position=Vector3(0, 0, 0),
                 rotation=Vector3(0, 0, 0), can_collide=False, gravity_factor=1,
                 kinematic=False, mass=1, color='Gazebo/Grey'):
        """
        Defines a state that creates a simple cylinder object in the world

        :param model_name: A given name for the new object that is spawned
        :param length: Number that specifies the length of the cylinder
        :param radius: Number that specifies the radius of the cylinder
        :param position: Vector3 with the initial position of the new object
        :param rotation: Vector3 in radians with the initial orientation fo the new object
        """
        geometry = '<cylinder><radius>' + str(radius) + '</radius><length>' \
                   + str(length) + '</length></cylinder>'
        super(SpawnCylinder, self).__init__(model_name, geometry, position, rotation, can_collide,
                                            gravity_factor, kinematic, mass, color)


class SpawnBox(ModelSpawnServiceState):
    """
    Defines a state that can spawn box objects
    """

    # pylint: disable=too-many-arguments
    def __init__(self, model_name, size=Vector3(1, 1, 1), position=Vector3(0, 0, 0),
                 rotation=Vector3(0, 0, 0), can_collide=False, gravity_factor=1,
                 kinematic=False, mass=1, color='Gazebo/Grey'):
        """
        Defines a state that creates a simple box object in the world

        :param model_name: A given name for the new object that is spawned
        :param size: A Vector3 that defines the three side lengths of the box
        :param position: Vector3 with the initial position of the new object
        :param rotation: Vector3 in radians with the initial orientation fo the new object
        """
        geometry = '<box><size>' + str(size.x) + ' ' + str(size.y) + ' ' \
                   + str(size.z) + '</size></box>'
        super(SpawnBox, self).__init__(model_name, geometry, position, rotation, can_collide,
                                       gravity_factor, kinematic, mass, color)


class DestroyModel(ModelDestroyServiceState):
    """
    Defines a state that can destroy models
    """

    def __init__(self, model_name):
        """
        Destroy a specific object

        :param model_name: The object's name that should be destroyed
        """
        super(DestroyModel, self).__init__(model_name)


class SetModelPose(SetModelServiceState):
    """
    Defines a state that can move/rotate/scale an object
    """

    def __init__(self, model_name, position, rotation, scale):
        """
        Change position/rotation/scaling of an object

        :param model_name: Name of the object to manipulate
        :param position: Vector3 to set the position of an object
        :param rotation: Quaternion to set the orientation of an object
        :param scale: Vector3 to set the scale of an object
        """
        super(SetModelPose, self).__init__(model_name, position, rotation, scale)


class TransformModelState(smach_ros.ServiceState):
    """
    Defines a service state that can transform models with translation, rotation and scaling
    """

    # pylint: disable=unused-argument
    def __request_callback(self, userdata, request):  # pragma: no cover
        """
        Callback when model transformation should be changed

        :param userdata: The simulation user data
        :param request: The original request
        """

        return self.create_service_request()

    def create_service_request(self):
        """
        Creates the service request object
        """

        # wait until we got current status information of the object to transform
        get_model_state = None
        try:
            while not rospy.is_shutdown() and get_model_state is None:
                if self.preempt_requested():  # pragma: no cover
                    rospy.loginfo("Preempting while waiting for service "
                                  "'/gazebo/get_model_state'")
                    self.service_preempt()
                    return None
                try:
                    rospy.wait_for_service(self._service_name, 1.0)
                    get_model_state = rospy.ServiceProxy('/gazebo/get_model_state',
                                                         GetModelState)
                    rospy.logdebug("Connected to service '/gazebo/get_model_state'")
                # pylint: disable=unused-variable
                except rospy.ROSException as ex:  # pragma: no cover
                    rospy.logwarn("Still waiting for service '/gazebo/get_model_state'")
        # pylint: disable=broad-except
        except Exception:  # pragma: no cover
            rospy.logwarn("Terminated while waiting for service "
                          "'/gazebo/get_model_state'")
            return None

        model_state = get_model_state(model_name=self.__name)

        model_state.pose.position.x += self.__translate.x
        model_state.pose.position.y += self.__translate.y
        model_state.pose.position.z += self.__translate.z

        # convert euler angles to quaternion rotation
        orientation = [model_state.pose.orientation.x,
                       model_state.pose.orientation.y,
                       model_state.pose.orientation.z,
                       model_state.pose.orientation.w]
        rotation = tf.transformations.quaternion_from_euler(self.__rotate.x,
                                                            self.__rotate.y,
                                                            self.__rotate.z)
        new_orientation = tf.transformations.quaternion_multiply(orientation, rotation)
        model_state.pose.orientation = Quaternion(*new_orientation)

        model_state.scale.x *= self.__scale.x
        model_state.scale.y *= self.__scale.y
        model_state.scale.z *= self.__scale.z

        return SetModelStateRequest(
            ModelState(model_name=self.__name, pose=model_state.pose, scale=model_state.scale))

    def __init__(self, model_name, translate=Vector3(0, 0, 0), rotate=Vector3(0, 0, 0),
                 scale=Vector3(1, 1, 1)):
        """
        Creates a new service state to transform an object via translation, rotation and scale

        :param model_name: Name of the object to manipulate
        :param position: Vector3 to translate the object
        :param rotation: Vector3 to rotate the object
        :param scale: Vector3 to rescale the object
        """
        super(TransformModelState, self).__init__('/gazebo/set_model_state', SetModelState,
                                                  request_cb=self.__request_callback)
        self.__name = model_name
        self.__translate = translate
        self.__rotate = rotate
        self.__scale = scale


class TranslateModelState(TransformModelState):
    """
    Defines a service state that can move objects according to world space
    """

    def __init__(self, model_name, x, y, z):
        """
        Creates a new service state to translate an object

        :param model_name: The name of the model object that has to be changed
        :param x: Amount of change according to x axis
        :param y: Amount of change according to y axis
        :param z: Amount of change according to z axis
        """
        super(TranslateModelState, self).__init__(model_name, translate=Vector3(x, y, z))


class RotateModelState(TransformModelState):
    """
    Defines a service state that can rotate objects according to world space
    """

    def __init__(self, model_name, roll, pitch, yaw):
        """
        Creates a new service state to rotate an object with given radian according to world space

        :param model_name: The name of the model object that has to be changed
        :param x: Amount of change according to x axis
        :param y: Amount of change according to y axis
        :param z: Amount of change according to z axis
        """
        super(RotateModelState, self).__init__(model_name,
                                               rotate=Vector3(roll, pitch, yaw))


class ScaleModelState(TransformModelState):
    """
    Defines a service state that can scale objects according to world space
    """

    def __init__(self, model_name, x, y, z):
        """
        Creates a new service state to scale an object

        :param model_name: The name of the model object that has to be changed
        :param x: Amount of change according to x axis
        :param y: Amount of change according to y axis
        :param z: Amount of change according to z axis
        """
        super(ScaleModelState, self).__init__(model_name, scale=Vector3(x, y, z))
