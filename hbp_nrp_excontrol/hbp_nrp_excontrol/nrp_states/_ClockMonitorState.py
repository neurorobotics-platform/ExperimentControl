# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains a state that monitors the world clock
"""

import rospy
import smach_ros
from rosgraph_msgs.msg import Clock

__author__ = 'Georg Hinkel'


def _to_rospy_time(t):
    """
    Convert argument from seconds to rospy.Time. Takes integer and float.
    For convenience rospy.Time is also accepted. In the latter case this
    operation is the identity.

    :param t: The time.
    :return: A rospy time instance.
    """
    if isinstance(t, rospy.Time):
        return t
    if isinstance(t, int):
        return rospy.Time(t)
    if isinstance(t, float):
        # Possible loss of precision from conversion to integer sec and ns.
        return rospy.Time.from_sec(t)

    raise ValueError("Unable to convert from type %s to rospy.Time." % str(type(t)))


def _to_rospy_duration(t):
    """
    Analogous to _to_rospy_time but returns rospy.Duration. Because rospy.Time
    dos not support the + operator ...

    :param t: The time.
    :return: A rospy duration instance
    """
    return _to_rospy_time(t) - rospy.Time(0)


class ClockMonitorState(smach_ros.MonitorState):
    """
    Defines a state that monitors the simulation time
    """

    def __init__(self, callback):
        """
        Creates a new state that monitors the simulation time

        :param callback: The callback that should be executed
        """
        super(ClockMonitorState, self).__init__('/clock', Clock, callback)


class WaitToClockState(ClockMonitorState):
    """
    Defines a state that waits until the clock has passed a certain threshold
    """

    # pylint: disable=unused-argument
    def __callback(self, user_data, time):
        """
        Callback when a new world clock time is available

        :param user_data: The user data of the state machine
        :param time: The world time
        """
        return time.clock < self.__threshold

    def __init__(self, threshold):
        """
        Defines a state that waits until the clock passes the given threshold

        :param threshold: The time threshold
        """
        super(WaitToClockState, self).__init__(self.__callback)
        self.__threshold = _to_rospy_time(threshold)


class ClockDelayState(ClockMonitorState):
    """
    Defines a state that waits until the clock has passed a certain threshold
    """

    # pylint: disable=unused-argument
    def __callback(self, user_data, time):
        """
        Callback when a new world clock time is available

        :param user_data: The user data of the state machine
        :param time: The world time
        """
        if self.__timeout is None:
            self.__timeout = time.clock + self.__waittime
        if time.clock < self.__timeout:
            return True
        self.__timeout = None
        return False

    def __init__(self, waittime):
        """
        Defines a state that waits until the clock passes the given threshold

        :param threshold: The time threshold
        """
        super(ClockDelayState, self).__init__(self.__callback)
        self.__waittime = _to_rospy_duration(waittime)
        self.__timeout = None
