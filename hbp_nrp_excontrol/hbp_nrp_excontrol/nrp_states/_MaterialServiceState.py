# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains classes to set materials in the virtual environment
"""

import smach_ros
from gazebo_msgs.srv import SetVisualProperties
from gazebo_msgs.srv import SetVisualPropertiesRequest

__author__ = 'Georg Hinkel'


class SetMaterialColorServiceState(smach_ros.ServiceState):
    """
    This state sets the color of materials in the virtual environment
    """

    def __init__(self, object_name, part_name, surface_name, color):
        """
        Sets the color of the given material

        :param object_name: The object whose material should be modified
        :param part_name: The part of the object that should be modified
        :param surface_name: The surface that should get a new color
        :param color: The color to set
        """
        super(SetMaterialColorServiceState, self).__init__('/gazebo/set_visual_properties',
                                                           SetVisualProperties,
                                                           request=SetVisualPropertiesRequest(
                                                               object_name,
                                                               part_name,
                                                               surface_name,
                                                               'material:script:name',
                                                               color
                                                           ))
