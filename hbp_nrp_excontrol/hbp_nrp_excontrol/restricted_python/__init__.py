"""
This package contains the restrictedPython access control for the project
"""

from hbp_nrp_excontrol.restricted_python._inplacevar import protected_inplacevar

_inplacevar_ = protected_inplacevar
