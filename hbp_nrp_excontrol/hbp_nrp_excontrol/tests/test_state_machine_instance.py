# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
__author__ = 'Georg Hinkel'

from hbp_nrp_excontrol.StateMachineInstance import StateMachineInstance
import unittest
import os
from unittest.mock import patch, Mock, MagicMock
from threading import Event
from cle_ros_msgs.srv import TriggerResponse


class TestExperimentStateMachine(unittest.TestCase):

    def setUp(self):
        self.rospy_patch = patch("hbp_nrp_excontrol.StateMachineInstance.rospy")
        self.rospy_mock = self.rospy_patch.start()

    def tearDown(self):
        self.rospy_patch.stop()

    def test_create_instance(self):
        error_pub = Mock()
        instance = StateMachineInstance("test_id", 0, error_pub)
        self.assertEquals(instance.sm_id, "test_id")
        self.assertEqual(instance.sim_id, 0)
        self.assertFalse(instance.is_running)
        self.assertFalse(instance.is_started)
        self.assertFalse(instance.has_error)

class TestExperimentStateMachineInternals(unittest.TestCase):

    def setUp(self):

        self.rospy_patch = patch("hbp_nrp_excontrol.StateMachineInstance.rospy")
        self.popen_patch = patch("hbp_nrp_excontrol.StateMachineInstance.subprocess.Popen")

        rospy_mock = self.rospy_patch.start()
        popen_mock = self.popen_patch.start()

        directory = os.path.split(__file__)[0]

        self.subprocess_mock = Mock()
        popen_mock.return_value = self.subprocess_mock
        self.proxy_mock = rospy_mock.ServiceProxy
        self.subscriber_mock = rospy_mock.Subscriber

        m = Mock(return_value=[True, ""])
        self.proxy_mock.return_value = m

        self.error_publisher = Mock()
        sm_path = os.path.join(directory, 'state_machine_mock.py')
        self.instance = StateMachineInstance("test_id", 0, self.error_publisher, sm_path)
        self.mocked_os = patch("hbp_nrp_excontrol.StateMachineInstance.os").start()
        self.mocked_os.path.join.return_value = "/some/tmp/dir/"

    def tearDown(self):
        self.rospy_patch.stop()
        self.popen_patch.stop()

    def __set_poll(self, value):
        self.subprocess_mock.poll.return_value = value

    def __mock_return(self, success, message):
        result = TriggerResponse(success, message)
        mock = Mock(return_value=result)
        self.proxy_mock.return_value = mock

    def test_is_running(self):
        self.instance.initialize_sm()
        self.__set_poll(None)
        self.assertTrue(self.instance.is_running)
        self.assertFalse(self.instance.is_started)
        self.__set_poll(0)
        self.assertFalse(self.instance.is_running)
        self.assertFalse(self.instance.is_started)

    @property
    def pyFile(self):
        return __file__[:-1] if __file__.endswith('.pyc') and os.path.exists(__file__[:-1]) else __file__

    def test_set_path(self):
        self.__mock_return(True, "")
        self.__set_poll(0)
        old_path = self.instance.sm_path
        self.instance.sm_path = self.pyFile
        self.instance.initialize_sm()
        self.instance.initialize_sm = Mock()
        self.__set_poll(None)
        self.assertFalse(self.instance.has_error)

    def test_set_path_syntax_error(self):
        self.instance.initialize_sm()
        self.__set_poll(None)
        self.__mock_return(True, "")

        self.instance.initialize_sm = Mock()
        directory = os.path.split(__file__)[0]
        broken_sm = os.path.join(directory, "state_machine_mock_syntax_error.txt")

        def set_to_broken_sm():
            self.instance.sm_path = broken_sm

        self.instance.error_publisher =  Mock()
        self.instance.error_publisher.return_value.publish.return_value = None
        self.assertRaises(SyntaxError, set_to_broken_sm)
        # check that the error message on the error topic has been sent
        self.assertTrue(self.instance.error_publisher.publish.called)
        # check that the sm's has_error flag is set to True
        self.assertTrue(self.instance.has_error)

    def test_set_path_syntax_error_no_publisher(self):
        directory = os.path.split(__file__)[0]
        sm_path = os.path.join(directory, 'state_machine_mock.py')
        instance = StateMachineInstance("test_id", 0, sm_path)

        instance.initialize_sm()
        self.__set_poll(None)
        self.__mock_return(True, "")

        instance.initialize_sm = Mock()
        broken_sm = os.path.join(directory, "state_machine_mock_syntax_error.txt")

        def set_to_broken_sm():
            instance.sm_path = broken_sm

        self.assertRaises(SyntaxError, set_to_broken_sm)
        # check that the sm's has_error flag is set to True
        self.assertTrue(instance.has_error)

    def test_poll(self):
        self.__set_poll(42)
        self.assertIsNone(self.instance.poll())
        self.instance.initialize_sm()
        self.assertEqual(42, self.instance.poll())

    def test_initialize(self):
        self.__mock_return(True, "")
        self.instance.initialize_sm()

        m = self.proxy_mock.return_value
        self.assertEqual(5, self.proxy_mock.call_count)
        self.assertEqual(5, m.wait_for_service.call_count)

        self.__set_poll(None) # is_running
        self.assertRaises(Exception, self.instance.initialize_sm)

        self.__set_poll(0) # not is_running
        self.instance.has_error = True # has_error
        self.assertRaises(Exception, self.instance.initialize_sm)

    def test_result(self):
        self.__mock_return(True, "Foobar")
        self.instance.initialize_sm()
        self.assertIsNone(self.instance.result)
        self.__set_poll(None)
        self.assertEqual("Foobar", self.instance.result)

    def test_start(self):
        self.__mock_return(True, "")

        m = self.proxy_mock.return_value
        self.instance.initialize_sm()

        self.__set_poll(0)
        self.assertRaises(Exception, self.instance.start_execution)

        self.__set_poll(None)
        self.assertFalse(m.called)
        self.instance.start_execution()
        self.assertTrue(m.called)
        self.assertTrue(self.instance.is_started)
        self.instance.start_execution(False)
        self.assertRaises(Exception, self.instance.start_execution, True)

    def test_restart(self):
        self.__mock_return(True, "")

        m = self.proxy_mock.return_value
        self.instance.initialize_sm()

        self.__set_poll(0)
        self.instance.initialize_sm = Mock()
        self.instance.restart()
        self.assertFalse(m.called)
        self.assertTrue(self.instance.initialize_sm.called)

        self.__set_poll(None)
        self.assertFalse(m.called)
        self.instance.restart()
        self.assertTrue(m.called)

    def test_request_termination(self):
        self.__mock_return(True, "")

        m = self.proxy_mock.return_value
        self.instance.initialize_sm()
        self.assertFalse(m.called)

        self.__set_poll(0)
        self.instance.request_termination()
        self.assertFalse(m.called)

        self.__set_poll(None)
        self.instance.request_termination()
        self.assertTrue(m.called)

    def test_wait_termination_signal(self):
        done_event = Event()
        self.__set_poll(None)
        self.subprocess_mock.wait = done_event.wait

        def send_signal_mock(signal):
            done_event.set()

        self.subprocess_mock.send_signal = send_signal_mock

        self.instance.initialize_sm()
        self.instance.wait_termination(0.1)

        self.assertFalse(self.subprocess_mock.terminate.called)
        self.assertFalse(self.subprocess_mock.kill.called)

    def test_wait_termination_terminate(self):
        done_event = Event()
        self.__set_poll(None)
        self.subprocess_mock.wait = done_event.wait

        self.subprocess_mock.terminate = done_event.set

        self.instance.initialize_sm()
        self.instance.wait_termination(0.1)

        self.assertTrue(self.subprocess_mock.send_signal.called)
        self.assertFalse(self.subprocess_mock.kill.called)

    def test_wait_termination_kill(self):
        done_event = Event()
        self.__set_poll(None)
        self.subprocess_mock.wait = done_event.wait

        self.subprocess_mock.kill = done_event.set

        self.instance.initialize_sm()
        self.instance.wait_termination(0.1)

        self.assertTrue(self.subprocess_mock.send_signal.called)
        self.assertTrue(self.subprocess_mock.terminate.called)

if __name__ == '__main__':
    unittest.main()
