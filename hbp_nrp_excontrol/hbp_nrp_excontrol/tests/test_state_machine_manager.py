# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module tests the state machine manager
"""

__author__ = 'Georg Hinkel'

import unittest
from unittest.mock import Mock, patch
from hbp_nrp_excontrol.StateMachineManager import StateMachineManager

class TestStateMachineManager(unittest.TestCase):

    def setUp(self):
        self.rospy_patch = patch("hbp_nrp_excontrol.StateMachineManager.rospy")
        self.rospy_mock = self.rospy_patch.start()

    def test_create_instance(self):
        sm_man = StateMachineManager()

        # empty sm list
        self.assertFalse(sm_man.state_machines)
        # publisher
        self.assertTrue(self.rospy_mock.Publisher.called)

    def tearDown(self):
        self.rospy_patch.stop()

class TestStateMachineManagerInternals(unittest.TestCase):

    def setUp(self):
        self.rospy_patch = patch("hbp_nrp_excontrol.StateMachineManager.rospy")
        self.rospy_mock = self.rospy_patch.start()

        self.foo = Mock()
        self.foo.sm_id = "foo"
        self.bar = Mock()
        self.bar.sm_id = "bar"

        self.sm_man = StateMachineManager()
        self.sm_man.state_machines.append(self.foo)
        self.sm_man.state_machines.append(self.bar)

    def tearDown(self):
        self.rospy_patch.stop()

    def test_get_state_machine(self):
        self.assertEqual(self.foo, self.sm_man.get_state_machine("foo"))
        self.assertEqual(self.bar, self.sm_man.get_state_machine("bar"))
        self.assertIsNone(self.sm_man.get_state_machine("not_existing"))

    @patch("hbp_nrp_excontrol.StateMachineManager.StateMachineInstance")
    def test_add_all(self, smi_mock):
        sm_mock = Mock()
        smi_mock.return_value = sm_mock
        new_paths = {
            "foobar": "foo_bar.py"
        }
        self.sm_man.add_all(new_paths, 0, "/sim/dir")
        self.assertTrue(smi_mock.called_once_with("foobar", "foo_bar.py", 0, "/sim/dir"))
        self.assertTrue(sm_mock in self.sm_man.state_machines)
        self.assertFalse(sm_mock.cle_error_pub is None)

        sm_mock.sm_id = "foobar"
        sm_mock.sm_path = "foo_bar.py"
        self.assertRaises(Exception, self.sm_man.add_all, new_paths)

    def test_create_state_machine(self):
        sm_id = "sm_foo"
        sim_id = 100
        sim_dir = "dir/file"
        name = "state_machine_name"
        new_sm = self.sm_man.create_state_machine(sm_id, sim_id, sim_dir)

        self.assertEqual(sm_id, new_sm.sm_id)
        self.assertEqual(sim_id, new_sm.sim_id)
        self.assertEqual(new_sm, self.sm_man.get_state_machine(sm_id))

        self.sm_man.state_machines.remove(new_sm)

    def test_shutdown(self):
        self.foo.is_running = True
        self.bar.is_running = False

        self.sm_man.shutdown()

        self.assertTrue(self.rospy_mock.Publisher().unregister.called)

        self.assertTrue(self.foo.request_termination.called)
        self.assertFalse(self.bar.request_termination.called)
        self.assertTrue(self.foo.wait_termination.called)
        self.assertFalse(self.bar.wait_termination.called)


    def test_terminate_all(self):
        self.foo.is_running = True
        self.bar.is_running = False
        self.sm_man.terminate_all()
        self.assertTrue(self.foo.request_termination.called)
        self.assertFalse(self.bar.request_termination.called)
        self.assertTrue(self.foo.wait_termination.called)
        self.assertFalse(self.bar.wait_termination.called)

    def test_restart_all(self):
        self.foo.is_running = True
        self.bar.is_running = False
        self.sm_man.restart_all()
        self.assertTrue(self.foo.restart.called)
        self.assertTrue(self.bar.restart.called)

    def test_start_all(self):
        self.foo.is_running = True
        self.bar.is_running = False
        self.sm_man.start_all()
        self.assertTrue(self.foo.start_execution.called)
        self.assertTrue(self.bar.start_execution.called)
        self.assertFalse(self.foo.initialize_sm.called)
        self.assertTrue(self.bar.initialize_sm.called)

    def test_initialize_all(self):
        self.sm_man.initialize_all()
        self.assertTrue(self.foo.initialize_sm.called)
        self.assertTrue(self.bar.initialize_sm.called)

if __name__ == '__main__':
    unittest.main()