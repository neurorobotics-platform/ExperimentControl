"""
This package contains the custom python code about logging
"""

__author__ = 'Claudio Sousa'

from hbp_nrp_excontrol.logs._ClientLogger import ClientLogger

clientLogger = ClientLogger()
