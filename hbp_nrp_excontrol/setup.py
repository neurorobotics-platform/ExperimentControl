'''setup.py'''
# pylint: disable=R0801
import os

from setuptools import setup

import hbp_nrp_excontrol


def parse_reqs(reqs_file):
    ''' parse the requirements '''
    install_reqs = list(val.strip() for val in open(reqs_file))
    reqs = install_reqs
    return reqs


BASEDIR = os.path.dirname(os.path.abspath(__file__))
REQS = parse_reqs(os.path.join(BASEDIR, 'requirements.txt'))

EXTRA_REQS_PREFIX = 'requirements_'
EXTRA_REQS = {}
for file_name in os.listdir(BASEDIR):
    if not file_name.startswith(EXTRA_REQS_PREFIX):
        continue
    base_name = os.path.basename(file_name)
    (extra, _) = os.path.splitext(base_name)
    extra = extra[len(EXTRA_REQS_PREFIX):]
    EXTRA_REQS[extra] = parse_reqs(file_name)

config = {
    'description': 'Experiment control for the HBP Neurorobotics platform',
    'author': 'HBP neurorobotics',
    'url': 'http://neurorobotics.net',
    'author_email': 'neurorobotics@humanbrainproject.eu',
    'version': hbp_nrp_excontrol.__version__,
    'install_requires': REQS,
    'extras_require': EXTRA_REQS,
    'packages': ['hbp_nrp_excontrol',
                 'hbp_nrp_excontrol.logs',
                 'hbp_nrp_excontrol.restricted_python',
                 'hbp_nrp_excontrol.nrp_states'],
    'classifiers': ["Programming Language :: Python :: 3"],
    'scripts': [],
    'name': 'hbp-nrp-excontrol',
    'include_package_data': True,
}

setup(**config)
