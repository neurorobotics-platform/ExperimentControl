import sys, os
# import conf.py from admin-scripts
sys.path.insert(0, os.environ.get('HBP') + '/admin-scripts/ContinuousIntegration/python/docs')
from sphinxconf import * # pylint: disable=import-error

# import modules needed by this project
sys.path.insert(0, os.path.abspath('../../hbp_nrp_excontrol'))  # Source code dir relative to this file
import hbp_nrp_excontrol

# -- General configuration -----------------------------------------------------
# Add any Sphinx extension module names here, as strings. 
# The common extensions are defined in admin-scripts
# extensions = []

apidoc_module_dir = '../../hbp_nrp_excontrol'

# General information about the project.
project = u'Experiment Control'

# -- Options for manual page output --------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'hbp_nrp_excontrol', u'Documentation for the Experiment Control',
     [u'Sharma', u'Akl'], 1)
]

# -- Options for Texinfo output ------------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    ('index', 'hbp_nrp_excontrol', u'Documentation for the Experiment Control',
     u'hinkel', 'hbp_nrp_excontrol',
     'The Experiment Control framework used in the HBP Neurorobotics Platform',
     'Miscellaneous'),
]

# -- Mocking for importing external modules ------------------------------------

# the following modules are part of CLE and should be mocked in the ExDBackend
autodoc_mock_imports = ['pyNN.nest', 'nest']


